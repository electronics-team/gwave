;
; module providing standard "toolbar" buttons for gwave
;

(define-module (app gwave std-toolbar)
  :use-module (app gwave cmds)
  :use-module (app gwave globals)
)

(dbprint "std-toolbar.scm running\n")
(set! gwave-std-toolbar-loaded #t)

(add-hook! 
 new-wavewin-hook
 (lambda ()
   (display "in std-toolbar.scm new-wavewin-hook") (newline)

   (toolbar-add #f "Zoom In"   (lambda () (x-zoom-relative! 2)))
   (toolbar-add #f "Zoom Out"  (lambda () (x-zoom-relative! 0.5)))
   (toolbar-add #f "Delete"    (lambda () (delete-selected-waves!)))
   (toolbar-add #f "Reload All"  reload-all-files!)
))

(dbprint "std-toolbar.scm done\n")
