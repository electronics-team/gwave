;
; module providing standard menus for gwave
;

(define-module (app gwave std-menus)
  :use-module (app gwave cmds)
;  :use-module (app gwave export)
  :use-module (app gwave globals)
  :use-module (app gwave utils)
)

;(debug-enable 'debug)
(read-enable 'positions)

(dbprint "std-menus.scm running\n")

;*****************************************************************************
; globals and ancilary procedures related to the menus

(define var-list-submenu #f)

(define (rebuild-varlist-submenu!)
  (display "FIXME rebuild-varlist-submenu\n")
  )

;*****************************************************************************
; construct the actual menus for gwave.
;

; hook called when main window opened.  Main purpose is creating the menus.
(add-hook! 
 new-wavewin-hook
;(define ffoo #f)
;(set! ffoo
 (lambda ()
   (dbprint "in std-menus.scm new-wavewin-hook\n")

   (add-menu "File"
	     (list
	      (list "About GWave" show-about-window!)
	      (list "Read File..." (lambda () (with-selected-filename "Datafile to load"  'open  (lambda (fn) (load-wavefile! fn)))))
	      (list "Plot..." 
		    (lambda () (popup-plot-dialog (wtable-wavepanels))))
	      (list "" #f)
	      (list "Save Configuration as Script"
		     (lambda () (with-selected-filename "Scriptfile to write" 'save
				 (lambda (fn) (write-allrestore-script fn))
				 ; #:default "gwave.gw"
				 )))
	      (list "Execute Guile Script..." 
		     (lambda () (with-selected-filename "Guile script to run" 'open  execute-script)))
	      (list "" #f)
	      (list "Quit" (lambda () (gtk-main-quit)))
	      ))
   
   (add-menu "View"
	     (list 
	      (list "Add Panel" 
		     (lambda () (wtable-insert-typed-panel! #f default-wavepanel-type)))
	      (list "Zoom Cursors" x-zoom-cursors!)
	      (list "Zoom X Full" x-zoom-full!)
	      (list "Zoom X..." x-zoom-area!)
	      (list "Zoom Y..." y-zoom-range!)
	      (list "Zoom XY-Area..." xy-zoom-area!)
	     ; (set! var-list-submenu (menu-create view-menu "Variable List"))
	      (list "Redraw All" wtable-redraw!)
	      )
       )

   (add-menu "Options"
	     (list 
	      (list "" #f)
					; FIXME how will we do radio-button submenus?
	      ))
   (dbprint "done std-menus.scm new-wavewin-hook\n")
   #f
   )
 )

;
; new-wavefile hook: add item to the variable-list menu for the file.
;
(add-hook! 
 new-wavefile-hook
 (lambda (df)
   (dbprint "in std-menus new-wavefile-hook " df "\n")

;   (add-menuitem var-list-submenu 
;		 (string-append (wavefile-tag df)
;				": "
;				(wavefile-file-name df))
;		 (lambda () (wavefile-show-listwin! df)))
   ) #t )

(add-hook!
 new-wavelist-hook
 (lambda (df)

   (add-wavelist-menu df "File"
	     (list
	      (list "Reload this file" (lambda () (wavefile-reload! df)))
;	      (list "Export Data..." (lambda () 
;				       (popup-export-dialog (wavefile-all-variables df))))
	      (list "Unload this File" (lambda ()
					 (wavefile-delete! df)
					 (rebuild-varlist-submenu!)
					 (wtable-redraw!)))
	      (list "Save Configuration as Script"
		    (lambda () (with-selected-filename "Scriptfile to write"  'open
						       (lambda (fn) (write-filerestore-script df fn)))))
   ;; 				 #:default (string-append 
    ;; 					    (wavefile-file-name df) ".gw"))))

	      (list "Apply Script to File"
		    (lambda () (with-selected-filename "Guile script to run" 'save
						       (lambda (fn) (apply-script-to-file fn df)))))
	      (list "" #f)
	      (list "Close" (lambda () (wavefile-remove-listwin! df)))
	     )
   )))

; Popup menu on button 3 in a wavepanel.  
; Note that the menu is constructed anew each time, so that it can be
; context-sensitive.
(wavepanel-bind-mouse 3
 (lambda (wp modstate time)
   (let ((button 3)
	 (next-ptype (remainder (+ 1 (wavepanel-type wp)) wavepanel-num-types)))
;   (display "in wavepanel menu ")(display wp) 
;   (display " type=") (display (wavepanel-type wp))
;   (dbprint " modstate=" modstate)
;   (dbprint " time=" time "\n")

   (gwgtk-menu-popup
    (list
     (list "Zoom Cursors" x-zoom-cursors!)
     (list "Zoom X..." x-zoom-area!)
     (list "Zoom X Full" x-zoom-full!)
     (list "Zoom Y..." y-zoom-range!)
     (list "Zoom Y Full+Auto" (lambda () (y-zoom-fullauto! wp)))
     (list "Zoom XY-Area..." xy-zoom-area!)
     (list "Zoom Dialog..." (lambda () (show-zoom-dialog! wp)))
     (list "Insert Panel Above" 
	   (lambda () (wtable-insert-typed-panel! wp default-wavepanel-type)))
     (list "Delete this Panel"
	   (lambda () (wtable-delete-panel! wp)))
     (list "Plot..." 
	   (lambda () (popup-plot-dialog (list wp))))

  ;; TODO rather than context-sensitive these ought to be sub-menus showing all choices.
     (list (string-append "Set type " (list-ref wavepanel-type-names next-ptype))
	   (lambda () (set-wavepanel-type! wp next-ptype)))

      (if (wavepanel-ylogscale? wp)
    	  (list "Linear Y Scale"
 		   (lambda () (set-wavepanel-ylogscale! wp #f)))
	  (list "Log Y Scale"
 		   (lambda () (set-wavepanel-ylogscale! wp #t))))
     
     ) button time)
   )))
   
(dbprint "std-menus.scm done\n")
