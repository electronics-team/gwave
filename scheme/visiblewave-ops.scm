;
; module providing GUI operations on VisibleWave objects
;   popup menu on right button in VisibleWave-button
;   multi-paneled dialog box accessible from the menu
;

(dbprint "visiblewave-ops.scm running\n")

(define-module (app gwave visiblewave-ops)
  :use-module (ice-9 format)
  :use-module (app gwave utils)
  :use-module (app gwave std-menus)
;  :use-module (app gwave export)
  :use-module (app gwave cmds)
  :use-module (app gwave globals)
)
(read-set! keywords 'prefix)

;(debug-enable 'debug)
(read-enable 'positions)

; replacement for statistics pane of the visible-wave options dialog
(define (popup-vw-stats vw)
  (popup-text-dialog "wave stats"
		     (string-append
		      (format #f "file:     ~s\n" ((wavefile-file-name visiblewave-file vw)))
		      (format #f "variable: ~s\n" (visiblewave-varname vw))
		      (format #f "minimum:  ~f\n" (wavevar-min vw))
		      (format #f "maximum:  ~f\n" (wavevar-max vw)))))
		      
  

; hook called when new VisibleWave is added.
(add-hook! 
 new-visiblewave-hook
 (lambda (vw)
   (display "new visiblewave hook\n")
   (dbprint "in exp new-visiblewave-hook " vw
	     "\n        file=" (visiblewave-file vw)
	     "\n     varname=" (visiblewave-varname vw)
	     "\n       panel=" (visiblewave-panel vw) "\n"  )

   (set-visiblewave-measure! vw 1 default-measure1-function)

   ; make this panel the only selected one when a wave is added to it
   (unselect-all-wavepanels!)
   (set-wavepanel-selected! (visiblewave-panel vw) #t)

   ; redraw on visiblewave-button click is wired into the C code for now
   (visiblewave-button3-connect vw
     (lambda (vw state time)
       (dbprint "in visiblewave button3 " vw)
       (dbprint " modstate=" state)
       (dbprint " time=" time "\n")

       (gwgtk-menu-popup
	(list
	 (list "Move to top" (lambda () (visiblewave-on-top! vw)))
	 (list "Stats..." (lambda () (popup-vw-stats vw)))
	 (list "Options" #f ) ; (lambda () (popup-vw-options vw)))
	 (list "Export..." #f ) ; (lambda () (popup-export-dialog (cons vw '()))))
	 (list "" #f)
	 (list "Delete" (lambda () (visiblewave-delete! vw)))
	 ) 3 time)
       ))
   ; TODO if X range is messed up, do zoom-x-full
   (dbprint "new-visiblewave-hook done\n")
))

			      
;;    (gtk-tooltips-set-tip gwave-tooltips (visiblewave-button vw)
;; 			 (string-append 
;; 			  (shorten-filename (wavefile-file-name (visiblewave-file vw)))
;; 			  ":\n"
;; 			  (visiblewave-varname vw)
;; 			  "\nVisibleWave Button:\nClick button 1 to select wave.\nPress button 3 for options menu.") "")
 
(wavepanel-bind-mouse 1
 (lambda (wp modstate time)
;   (format #t "wavepanel ~s modifiers=~s\n" wp modstate)

   (if (= 0 (logand 1 modstate))
     (unselect-all-wavepanels!))
   (set-wavepanel-selected! wp #t)
))


(dbprint "visiblewave-ops.scm done\n")
