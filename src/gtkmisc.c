/* gtkmisc.c - 
 * misc. generic helper routines for Gtk+, that aren't at all specfic to
 * this particular application. 
 *
 * these routines were borrowed from testgtk.c:
 *	shape_pressed
 *	shape_released
 *	shape_motion
 *	shape_create_icon
 *
 * Original copyright message from testgtk.c follows:
 *
 * GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gwave.  If not, see <http://www.gnu.org/licenses/>.
 *
 * I created shape_create_icon_d by making a trivial change to a copy
 * of shape_create_icon, and hearby disclaim all rights to that 
 * derivative work. - Stephen Tell
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <validate.h>
#include <scwm_guile.h>
#include <gwave.h>

/* A couple of routines to make it easier to build menus.
 * These are by Steve Tell.
 *
 * Create a (sub)menu and the item that activates it.
 * Returns GtkMenu widget pointer.
 */
GtkWidget *
create_menu(char *label, GtkWidget *parent)
{
	GtkWidget *item, *menu;
	
	if(label)
		item = gtk_menu_item_new_with_label(label);
	else
		item = gtk_menu_item_new();

	menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(item), menu);

	if(parent) {
		if(GTK_IS_MENU_BAR(parent))
			gtk_menu_bar_append (GTK_MENU_BAR (parent), item);
		else if(GTK_IS_MENU(parent))
			gtk_menu_append(GTK_MENU(parent), item);
	}
	gtk_widget_show(item);
	return menu;
}

/*
 * helper function for making menu items.  Returns menu item widget pointer,
 * but it can often be ignored, since the item is already added to the parent.
 */
GtkWidget *
create_menuitem(char *label, GtkWidget *parent, GtkSignalFunc action, 
		gpointer p)
{
	GtkWidget *item;
	if(label)
		item = gtk_menu_item_new_with_label(label);
	else
		item = gtk_menu_item_new();

	if(action)
		gtk_signal_connect (GTK_OBJECT (item), "activate", action, p);
	if(parent)
		gtk_menu_append (GTK_MENU(parent), item);
	gtk_widget_show (item);
	return item;
}


/*
 * pop up a view-only dialog with a line or lines of text from a null-terminated buffer.
 */
void
create_gtktextdialog(char *title, char *text)
{

	GtkTextBuffer *textbuf;
	GtkWidget *textview;
	GtkWidget *dialog_content;
	GtkDialog *dialog;

	textbuf = gtk_text_buffer_new(NULL);
	gtk_text_buffer_set_text(textbuf, text, -1);

	textview = gtk_text_view_new_with_buffer(textbuf);

	dialog = GTK_DIALOG(gtk_dialog_new_with_buttons(title,
							NULL, 0,
							GTK_STOCK_OK,
							GTK_RESPONSE_ACCEPT, NULL));

	g_signal_connect_swapped (dialog, "response",
                          G_CALLBACK (gtk_widget_destroy),
                          dialog);

	dialog_content = gtk_dialog_get_content_area(dialog);
	gtk_container_set_border_width (GTK_CONTAINER(dialog_content), 10);
	gtk_container_add (GTK_CONTAINER(dialog_content), textview); 
	
	gtk_widget_show (textview);
	gtk_widget_show (GTK_WIDGET(dialog));
}

/*
 * Shaped Windows
 */
static GdkWindow *root_win = NULL;

typedef struct _cursoroffset {gint x,y;} CursorOffset;

static void
shape_pressed (GtkWidget *widget, GdkEventButton *event)
{
  CursorOffset *p;

  /* ignore double and triple click */
  if (event->type != GDK_BUTTON_PRESS)
    return;

  p = gtk_object_get_user_data (GTK_OBJECT(widget));
  p->x = (int) event->x;
  p->y = (int) event->y;

  gtk_grab_add (widget);
  gdk_pointer_grab (widget->window, TRUE,
		    GDK_BUTTON_RELEASE_MASK |
		    GDK_BUTTON_MOTION_MASK |
		    GDK_POINTER_MOTION_HINT_MASK,
		    NULL, NULL, 0);
}


static void
shape_released (GtkWidget *widget)
{
  gtk_grab_remove (widget);
  gdk_pointer_ungrab (0);
}

static void
shape_motion (GtkWidget      *widget, 
	      GdkEventMotion *event)
{
  gint xp, yp;
  CursorOffset * p;
  GdkModifierType mask;

  p = gtk_object_get_user_data (GTK_OBJECT (widget));

  /*
   * Can't use event->x / event->y here 
   * because I need absolute coordinates.
   */
  gdk_window_get_pointer (root_win, &xp, &yp, &mask);
  gtk_widget_set_uposition (widget, xp  - p->x, yp  - p->y);
}

GtkWidget *
shape_create_icon (char     *xpm_file,
		   gint      x,
		   gint      y,
		   gint      px,
		   gint      py,
		   gint      window_type)
{
  GtkWidget *window;
  GtkWidget *pixmap;
  GtkWidget *fixed;
  CursorOffset* icon_pos;
  GdkGC* gc;
  GdkBitmap *gdk_pixmap_mask;
  GdkPixmap *gdk_pixmap;
  GtkStyle *style;

  style = gtk_widget_get_default_style ();
  gc = style->black_gc;	

  /*
   * GDK_WINDOW_TOPLEVEL works also, giving you a title border
   */
  window = gtk_window_new (window_type);
  
  fixed = gtk_fixed_new ();
  gtk_widget_set_usize (fixed, 100,100);
  gtk_container_add (GTK_CONTAINER (window), fixed);
  gtk_widget_show (fixed);
  
  gtk_widget_set_events (window, 
			 gtk_widget_get_events (window) |
			 GDK_BUTTON_MOTION_MASK |
			 GDK_POINTER_MOTION_HINT_MASK |
			 GDK_BUTTON_PRESS_MASK);

  gtk_widget_realize (window);
  gdk_pixmap = gdk_pixmap_create_from_xpm (window->window, &gdk_pixmap_mask, 
					   &style->bg[GTK_STATE_NORMAL],
					   xpm_file);

  pixmap = gtk_pixmap_new (gdk_pixmap, gdk_pixmap_mask);
  gtk_fixed_put (GTK_FIXED (fixed), pixmap, px,py);
  gtk_widget_show (pixmap);
  
  gtk_widget_shape_combine_mask (window, gdk_pixmap_mask, px,py);


  gtk_signal_connect (GTK_OBJECT (window), "button_press_event",
		      GTK_SIGNAL_FUNC (shape_pressed),NULL);
  gtk_signal_connect (GTK_OBJECT (window), "button_release_event",
		      GTK_SIGNAL_FUNC (shape_released),NULL);
  gtk_signal_connect (GTK_OBJECT (window), "motion_notify_event",
		      GTK_SIGNAL_FUNC (shape_motion),NULL);

  icon_pos = g_new (CursorOffset, 1);
  gtk_object_set_user_data(GTK_OBJECT(window), icon_pos);

  gtk_widget_set_uposition (window, x, y);
  gtk_widget_show (window);
  
  return window;
}


GtkWidget *
shape_create_icon_d (char     **xpm_data,
		     gint      x,
		     gint      y,
		     gint      px,
		     gint      py,
		     gint      window_type)
{
  GtkWidget *window;
  GtkWidget *pixmap;
  GtkWidget *fixed;
  CursorOffset* icon_pos;
  GdkGC* gc;
  GdkBitmap *gdk_pixmap_mask;
  GdkPixmap *gdk_pixmap;
  GtkStyle *style;

  style = gtk_widget_get_default_style ();
  gc = style->black_gc;	

  /*
   * GDK_WINDOW_TOPLEVEL works also, giving you a title border
   */
  window = gtk_window_new (window_type);
  
  fixed = gtk_fixed_new ();
  gtk_widget_set_usize (fixed, 100,100);
  gtk_container_add (GTK_CONTAINER (window), fixed);
  gtk_widget_show (fixed);
  
  gtk_widget_set_events (window, 
			 gtk_widget_get_events (window) |
			 GDK_BUTTON_MOTION_MASK |
			 GDK_POINTER_MOTION_HINT_MASK |
			 GDK_BUTTON_PRESS_MASK);

  gtk_widget_realize (window);
  gdk_pixmap = gdk_pixmap_create_from_xpm_d (window->window, &gdk_pixmap_mask, 
					   &style->bg[GTK_STATE_NORMAL],
					   xpm_data);

  pixmap = gtk_pixmap_new (gdk_pixmap, gdk_pixmap_mask);
  gtk_fixed_put (GTK_FIXED (fixed), pixmap, px,py);
  gtk_widget_show (pixmap);
  
  gtk_widget_shape_combine_mask (window, gdk_pixmap_mask, px,py);


  gtk_signal_connect (GTK_OBJECT (window), "button_press_event",
		      GTK_SIGNAL_FUNC (shape_pressed),NULL);
  gtk_signal_connect (GTK_OBJECT (window), "button_release_event",
		      GTK_SIGNAL_FUNC (shape_released),NULL);
  gtk_signal_connect (GTK_OBJECT (window), "motion_notify_event",
		      GTK_SIGNAL_FUNC (shape_motion),NULL);

  icon_pos = g_new (CursorOffset, 1);
  gtk_object_set_user_data(GTK_OBJECT(window), icon_pos);

  gtk_widget_set_uposition (window, x, y);
  gtk_widget_show (window);
  
  return window;
}

void gwgtk_guile_callback(GtkWidget *widget,  gpointer data)
{
	SCM proc = (SCM)data;
//	if(gwave_debug) {
//		printf("gwgtk_guile_callback\n");
//	}
	scwm_safe_call0(proc);
}

// build a GtkMenu from the scheme LIST of lists defining the items.
// common code used for several menus.
GtkWidget *
build_menu_from_scmlist(SCM mlist)
{
	GtkWidget *topitem;
	GtkWidget *menu;
	SCM s_mitem;
	int len, i;

//	topitem = gtk_menu_item_new_with_label(mname);
	menu = gtk_menu_new ();
	gtk_widget_show(menu);
//	gtk_menu_item_set_submenu(GTK_MENU_ITEM(topitem), menu);

	len = scm_ilength(mlist);
	for(i = 0; i < len; i++) {
		GtkWidget *menu_item;
		SCM s_itemlabel;
		SCM s_itemproc;
		char *item_label;
		SCM port = scm_current_output_port();
		s_mitem = scm_list_ref(mlist, scm_from_signed_integer(i));
		s_itemlabel = scm_car(s_mitem);
		s_itemproc = scm_cadr(s_mitem);
		 
//		scm_display(s_itemlabel, port);
//		scm_newline(port);
		if (!SCM_NFALSEP (scm_string_p(s_itemlabel))) {
			printf("add_menu item name not a string\n");
//			scm_wrong_type_arg(FUNC_NAME,2,s_itemlabel);
		}
		item_label = scm_to_locale_string(s_itemlabel);
//		if(gwave_debug) {
//			printf(".  %s\n", item_label);
//		}
		if(strlen(item_label) == 0) {
			menu_item = gtk_separator_menu_item_new();
		} else {
			menu_item = gtk_menu_item_new_with_label (item_label);
			if(SCM_NFALSEP(scm_procedure_p(s_itemproc)))
				g_signal_connect(G_OBJECT(menu_item), "activate",
					 GTK_SIGNAL_FUNC(gwgtk_guile_callback),
					 (gpointer)s_itemproc);
		}
		gtk_menu_append (GTK_MENU (menu), menu_item);
		gtk_widget_show (menu_item);
	}
	return menu;
}



SCM_DEFINE(gwgtk_menu_popup, "gwgtk-menu-popup", 3, 0, 0,
	   (SCM list, SCM button, SCM time),
	   "build a menu from the LIST of entries, and pop it up with gtk_menu_popup using mouse BUTTON and event TIME.")
#define FUNC_NAME s_gwgtk_menu_popup
{
        GtkWidget *menu;
	int mbutton, len;
	time_t activate_time;
	VALIDATE_ARG_LIST(1, list);
	VALIDATE_ARG_INT_COPY(2, button, mbutton);
	VALIDATE_ARG_INT_COPY(3, time, activate_time);
	
	len = scm_ilength(list);
	if(len <= 0)
		return SCM_UNSPECIFIED;

//	printf("gwgtk_menu_popup %d\n", mbutton);
	menu = build_menu_from_scmlist(list);
	gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, mbutton, activate_time);
}
#undef FUNC_NAME	   

SCM_DEFINE(with_selected_file, "with-selected-filename", 3, 0, 0,
	   (SCM title, SCM action, SCM proc),
	   "put up a file-section dialog with the indicated TITLE, and if the user selects a filename, call a PROC with it.")
#define FUNC_NAME s_with_selected_file
{
	char *ctitle;
	char *default_fname = NULL;
	GtkWidget *dialog;
	GtkFileChooserAction gtkaction;

	VALIDATE_ARG_STR_NEWCOPY_USE_NULL(1,title,ctitle);
	VALIDATE_ARG_SYM(2, action);
	VALIDATE_ARG_PROC(3, proc);

	// scm symbol action should be 'open or 'save
	// could also support 'folder or 'create_folder
	SCM sym_save = scm_from_latin1_symbol("save");
	if(scm_eq_p(action, sym_save))
	   gtkaction = GTK_FILE_CHOOSER_ACTION_SAVE;
	else
	   gtkaction = GTK_FILE_CHOOSER_ACTION_OPEN;

     	dialog = gtk_file_chooser_dialog_new ("Open File",
					      NULL,
					      gtkaction,
				      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				      NULL);

	
//	if(default_fname) {
//		gtk_file_selection_set_filename(GTK_FILE_SELECTION(file_selector), default_fname);
//	}
     
	if(gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *fname;
		SCM sfname;
		fname = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		if(gwave_debug)
			printf("with_selected_file_cb %s\n", fname);
                sfname = scm_from_locale_string(fname);
                scwm_safe_call1(proc, sfname);
		
		g_free (fname);
	}

	gtk_widget_destroy (dialog);

	return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


SCM_DEFINE(popup_text_dialog, "popup-text-dialog", 2, 0, 0,
	   (SCM title, SCM text),
	   "put up a dialog containing only the indicated informational text.")
#define FUNC_NAME s_popup_text_dialog
{
	char *ctext;
	char *ctitle;
	VALIDATE_ARG_STR_NEWCOPY_USE_NULL(1,title,ctitle);
	VALIDATE_ARG_STR_NEWCOPY_USE_NULL(2,text,ctext);
	create_gtktextdialog(ctitle, ctext);
}
#undef FUNC_NAME

SCM_DEFINE(wrap_gtk_main_quit, "gtk-main-quit", 0, 0, 0,
	   (),
	   "exit from Gtk program")
#define FUNC_NAME s_wrap_gwave_main_quit
{
	gtk_main_quit();
	return SCM_UNSPECIFIED; // notreached
}
#undef FUNC_NAME


/* guile initialization */
void init_gtkmisc()
{
#ifndef XSCM_MAGIC_SNARF_INITS
#include "gtkmisc.x"
#endif
}
