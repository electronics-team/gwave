/*
 * scwm_guile.c
 * Copyright (C) 1999 Steve Tell
 *
 * based heavily on callbacks.c from SCWM:
 * Copyright (C) 1997-1999 Maciej Stachowiak and Greg J. Badros
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gwave.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/time.h>
#include <unistd.h>
#include <limits.h>
#include <assert.h>

#include <libguile.h>
#include <libguile/fluids.h>

#include "scwm_guile.h"
#include "guile-compat.h"

#ifdef USE_DMALLOC
#include "dmalloc.h"
#endif

extern char *prog_name;

// debug aid. might leak memory.
void my_display(char *tag, SCM obj)
{
  SCM port = scm_current_output_port();
  scm_puts(tag, port);
  scm_puts(": ", port);
  scm_display(obj, port);
  scm_newline(port);
}

struct scwm_body_apply_data {
  SCM proc;
  SCM args;
};

static SCM
scwm_body_apply (void *body_data)
{
  struct scwm_body_apply_data *ad = (struct scwm_body_apply_data *) body_data;
  SCM rc;

//  my_display("scwm_body_apply proc", ad->proc);
//  my_display("scwm_body_apply args", ad->args);

  rc = scm_apply(ad->proc, ad->args, SCM_EOL);
//  my_display("scwm_body_apply retval", rc);
  return rc;
}

SCM 
scwm_handle_error (void *ARG_IGNORE(data), SCM tag, SCM throw_args)
{
  SCM port =  scm_current_error_port();

  scm_puts ("Throw to key ", port);
  scm_write (tag, port);
  scm_puts (" with args ", port);
  scm_write (throw_args, port);
  scm_puts ("\n", port);
  printf("FIXME scwm_handle_error\n");

  return SCM_BOOL_F;
}


/* Use some advanced libguile calls to establish a new dynamic root 
   this causes all throws to be caught and prevents continuations from exiting the
   dynamic scope of the callback. This is needed to prevent callbacks
   from disrupting the C program's flow control, which would likely cause a
   crash. 
*/

SCM
scwm_safe_apply (SCM proc, SCM args)
{
  struct scwm_body_apply_data apply_data;
  SCM port =  scm_current_output_port();
  SCM_STACKITEM stack_place;
  SCM rc;

  apply_data.proc = proc;
  apply_data.args = args;

  // TODO rewrite this to get stack so we can print a backtrace
  rc = scm_internal_cwdr(scwm_body_apply,
			   &apply_data,
			   scwm_handle_error, prog_name,
			   &stack_place);
  // my_display("scwm_safe_apply retval", rc);
  return rc;
}

SCM
scwm_safe_apply_message_only (SCM proc, SCM args)
{
  struct scwm_body_apply_data apply_data;

  apply_data.proc = proc;
  apply_data.args = args;

  return scm_internal_cwdr(scwm_body_apply, &apply_data,
			   scm_handle_by_message_noexit, prog_name,
			   NULL);
}


SCM
scwm_safe_call0 (SCM thunk)
{
  return scwm_safe_apply (thunk, SCM_EOL);
}


SCM
scwm_safe_call1 (SCM proc, SCM arg)
{
  return scwm_safe_apply (proc, scm_list_1(arg));
}


SCM
scwm_safe_call2 (SCM proc, SCM arg1, SCM arg2)
{
  /* This means w must cons (albeit only once) on each callback of
     size two - seems lame. */
  return scwm_safe_apply (proc, scm_list_2(arg1, arg2));
}

SCM
scwm_safe_call3 (SCM proc, SCM arg1, SCM arg2, SCM arg3)
{
  /* This means w must cons (albeit only once) on each callback of
     size two - seems lame. */
  return scwm_safe_apply (proc, scm_list_3(arg1, arg2, arg3));
}

SCM
scwm_safe_call4 (SCM proc, SCM arg1, SCM arg2, SCM arg3, SCM arg4)
{
  /* This means w must cons (albeit only once) on each callback of
     size two - seems lame. */
  return scwm_safe_apply (proc, scm_list_4(arg1, arg2, arg3, arg4));
}

SCM
scwm_safe_call5 (SCM proc, SCM arg1, SCM arg2, SCM arg3, SCM arg4, SCM arg5)
{
  /* This means w must cons (albeit only once) on each callback of
     size two - seems lame. */
  return scwm_safe_apply (proc, scm_list_5(arg1, arg2, arg3, arg4, arg5));
}

SCM
scwm_safe_call6 (SCM proc, SCM arg1, SCM arg2, SCM arg3, SCM arg4, SCM arg5, SCM arg6)
{
  return scwm_safe_apply (proc, scm_list_n(arg1, arg2, arg3, arg4, arg5, arg6, SCM_UNDEFINED));
}

SCM
scwm_safe_call7 (SCM proc, SCM arg1, SCM arg2, SCM arg3, SCM arg4, SCM arg5, SCM arg6, SCM arg7)
{
  return scwm_safe_apply (proc, scm_list_n(arg1, arg2, arg3, arg4, arg5, arg6, arg7, SCM_UNDEFINED));
}


static SCM run_hook_proc;

SCM scwm_run_hook(SCM hook, SCM args)
{
  
  return scwm_safe_apply(run_hook_proc, scm_cons(hook,args));
}

SCM scwm_run_hook_message_only(SCM hook, SCM args)
{
  return scwm_safe_apply_message_only(run_hook_proc, scm_cons(hook,args));
}


SCM call0_hooks(SCM hook)
{
  return scwm_run_hook(hook,SCM_EOL);
}

SCM call1_hooks(SCM hook, SCM arg1)
{
  return scwm_run_hook(hook,scm_list_1(arg1));
}

SCM call2_hooks(SCM hook, SCM arg1, SCM arg2)
{
  return scwm_run_hook(hook,scm_list_2(arg1,arg2));
}

SCM call3_hooks(SCM hook, SCM arg1, SCM arg2, SCM arg3)
{
  return scwm_run_hook(hook,scm_list_n(arg1,arg2,arg3,SCM_UNDEFINED));
}

SCM call4_hooks(SCM hook, SCM arg1, SCM arg2, SCM arg3, SCM arg4)
{
  return scwm_run_hook(hook,scm_list_n(arg1,arg2,arg3,arg4,SCM_UNDEFINED));
}

SCM call5_hooks(SCM hook, SCM arg1, SCM arg2, SCM arg3, SCM arg4, SCM arg5)
{
  return scwm_run_hook(hook,scm_list_n(arg1,arg2,arg3,arg4,arg5,SCM_UNDEFINED));
}

SCM call6_hooks(SCM hook, SCM arg1, SCM arg2, SCM arg3, SCM arg4, SCM arg5, SCM arg6)
{
  return scwm_run_hook(hook,scm_list_n(arg1,arg2,arg3,arg4,arg5,arg6,SCM_UNDEFINED));
}

SCM call7_hooks(SCM hook, SCM arg1, SCM arg2, SCM arg3, SCM arg4, SCM arg5, SCM arg6, SCM arg7)
{
  return scwm_run_hook(hook,scm_list_n(arg1,arg2,arg3,arg4,arg5,arg6,arg7,SCM_UNDEFINED));
}

SCM
scm_empty_hook_p(SCM hook)
{
  return scm_hook_empty_p(hook);
}




/* Slightly tricky - we want to catch errors per expression, but only
   establish a new dynamic root per load operation, as it's perfectly
   OK for a file to invoke a continuation created by a different
   expression in the file as far as scwm is concerned. So we set a
   dynamic root for the whole load operation, but also catch on each
   eval. */

static SCM
scwm_body_eval_x (void *body_data)
{
  SCM expr = *(SCM *) body_data;
  return scm_eval_x (expr, scm_current_module() );
}

static SCM
ss_handler (void *data SCM_UNUSED, SCM tag, SCM throw_args)
{
  /* In the stack */
  scm_fluid_set_x (scm_variable_ref
                   (scm_c_module_lookup
                    (scm_c_resolve_module ("ice-9 save-stack"),
                     "the-last-stack")),
                   scm_make_stack (SCM_BOOL_T, SCM_EOL));
  /* Throw the error */
  return scm_throw (tag, throw_args);
}

struct cwss_data
{
  SCM tag;
  scm_t_catch_body body;
  void *data;
};

static SCM
cwss_body (void *data)
{
  struct cwss_data *d = data;
  return scm_c_with_throw_handler (d->tag, d->body, d->data, ss_handler, NULL, 0
);
}


static SCM 
scwm_catching_eval_x (SCM expr) {
  /*
  return scm_internal_stack_catch (SCM_BOOL_T,
				   scwm_body_eval_x,
				   &expr,
				   scwm_handle_error,
				   prog_name);

SCM
scm_internal_stack_catch (SCM tag,
                          scm_t_catch_body body,
                          void *body_data,
                          scm_t_catch_handler handler,
                          void *handler_data)
  */
  
  struct cwss_data d;
  d.tag = SCM_BOOL_T;        // tag;
  d.body = scwm_body_eval_x; // body
  d.data = &expr;            // body_data;
//  return scm_internal_catch (tag, cwss_body, &d, handler, handler_data);

  return scm_internal_catch (SCM_BOOL_T,
			     cwss_body,
			     &d,
			     scwm_handle_error,
			     prog_name);
  
}

static SCM 
scwm_catching_load_from_port (SCM port)
{
  SCM expr;
  SCM answer = SCM_UNSPECIFIED;

  while (!SCM_EOF_OBJECT_P(expr = scm_read (port))) {  
    answer = scwm_catching_eval_x (expr);
  }
  scm_close_port (port);

  return answer;
}

static SCM
scwm_body_load (void *body_data)
{
  SCM filename = *(SCM *) body_data;
  SCM port = scm_open_file (filename, scm_from_latin1_string("r"));
  return scwm_catching_load_from_port (port);
}

static SCM
scwm_body_eval_str (void *body_data)
{
  char *string = (char *) body_data;
  SCM port = scm_mkstrport (scm_from_int(0), scm_from_latin1_string(string), 
			    SCM_OPN | SCM_RDNG, "scwm_safe_eval_str");
  return scwm_catching_load_from_port (port);
}



SCM_DEFINE(safe_load, "safe-load", 1, 0, 0,
           (SCM fname),
"Load file FNAME while trapping and displaying errors."
"Each individual top-level-expression is evaluated separately and all"
"errors are trapped and displayed.  You should use this procedure if"
"you need to make sure most of a file loads, even if it may contain"
"errors.")
#define FUNC_NAME s_safe_load
{
  SCM_STACKITEM stack_item;
  VALIDATE_ARG_STR(1,fname);
  return scm_internal_cwdr(scwm_body_load, &fname,
				     scm_handle_by_message_noexit, prog_name, 
				     &stack_item);
}
#undef FUNC_NAME

SCM scwm_safe_load (char *filename)
{
  return safe_load(scm_from_locale_string(filename));
}

SCM scwm_safe_eval_str (char *string)
{
  SCM_STACKITEM stack_item;
  return scm_internal_cwdr(scwm_body_eval_str, string,
				     scm_handle_by_message_noexit, prog_name, 
				     &stack_item);
}

void init_scwm_guile()
{
  run_hook_proc = SCM_VARIABLE_REF(scm_c_lookup("run-hook"));
  printf("run_hook_proc=%p\n", run_hook_proc);
  my_display("run_hook_proc", run_hook_proc);
  // assert( !SCM_NFALSEP (scm_procedure_p(run_hook_proc)));
  
#ifndef SCM_MAGIC_SNARF_INITS
#include "scwm_guile.x"
#endif
}


/* Local Variables: */
/* tab-width: 8 */
/* c-basic-offset: 2 */
/* End: */
/* vim:ts=8:sw=2:sta 
 */

