/* $Id: guile-compat.h,v 1.5 2002/01/10 04:04:26 sgt Exp $ */
/*
 * Original Copyright (C) 1997-2001, Maciej Stachowiak and Greg J. Badros
 * Additions copyright 2008 Stephen G. Tell.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gwave.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GUILE_COMPAT_H
#define GUILE_COMPAT_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define DEREF_LAST_STACK scm_fluid_ref(SCM_VARIABLE_REF (scm_the_last_stack_fluid_var))

SCM make_output_strport(char *fname);
extern char *safe_scm_to_stringn (SCM str, size_t *lenp);

extern long scwm_make_smob_type_mfpe (char *name, size_t size,
                        SCM (*mark) (SCM),
                        size_t (*free) (SCM),
                        int (*print) (SCM, SCM, scm_print_state *),
				      SCM (*equalp) (SCM, SCM));


#endif /* GUILE_COMPAT_H */

/* Local Variables: */
/* tab-width: 8 */
/* c-basic-offset: 2 */
/* End: */
/* vim:ts=8:sw=2:sta 
 */

