/* 
 * based on scwm arg_unused.h
 * (C) 1999, Greg J. Badros
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gwave.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARG_UNUSED_H__
#define ARG_UNUSED_H__

/* Can mark unused formals as ARG_IGNORE or ARG_UNUSED and avoid warning;
   uses a gcc feature, but C++ also can do this by just
   not giving a formal name.
   ARG_IGNORE is for arguments that really won't be used.
   whereas ARG_UNUSED just comments that the argument is not used at present
     and might be worth revisiting to see if we can generalize the code
     to use it. (Or if alternate implementations might use the variable) */
#ifdef __GNUC__
/* do not use the variable name as given-- paste an
   "_unused" on to the end so we force an error if
   it is used. */
#define ARG_IGNORE(x) x ## _ignore __attribute__ ((unused))
#define ARG_UNUSED(x) x ## _unused __attribute__ ((unused))
#elif defined(__cplusplus)
#define ARG_IGNORE(x) /* empty */
#define ARG_UNUSED(x) /* empty */
#else
#define ARG_IGNORE(x) x ## _ignore
#define ARG_UNUSED(x) x ## _unused
#endif


#endif
