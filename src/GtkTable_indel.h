/*
 * GtkTable_indel.h - GtkTable modification functions
*
 * This file is part of gwave.
 * Copyright (C) 2001-2008 Stephen G. Tell.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gwave.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GTK_TABLE_INDEL_H
#define GTK_TABLE_INDEL_H

extern void gtk_table_move_row(GtkTable *table, int from, int to);
extern void gtk_table_insert_row(GtkWidget *widget, int row);
extern void gtk_table_delete_row(GtkWidget *widget, int row);
extern void gtk_table_rotate_rows(GtkWidget *widget, int from, int to);
extern int gtk_table_get_child_row(GtkWidget *widget, GtkWidget *child);

#endif
